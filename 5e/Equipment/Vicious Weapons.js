// Midi-qol On use macro.
if((args[0].hitTargets.length === 1) && (args[0].isCritical)){
    let target = canvas.tokens.get(args[0].hitTargets[0]._id);
    let itemD = args[0].item;
    let damageType = args[0].damageDetail[0].type;
    let damageRoll = new Roll('2d6').roll();
    new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, damageType, [target], damageRoll, {flavor: `${itemD.name} - Damage Roll`, damageList: args[0].damageList, itemCardId: args[0].itemCardId});
}
