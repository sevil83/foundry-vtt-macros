//###########################################################################################
// Read First!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MidiQOL "on use" macro
//###########################################################################################
if(args[0].hitTargets.length > 0){
    let actorD = game.actors.get(args[0].actor._id);
    let target = canvas.tokens.get(args[0].hitTargets[0]._id);
    let itemD = args[0].item;
    let damageRoll;
    let giant = ["giant"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
    let numDice = args[0].isCritical ? "4d6" : "2d6";
    if (giant) damageRoll = new Roll(numDice).roll();
    let damageType = args[0].damageDetail[0].type;
    new MidiQOL.DamageOnlyWorkflow(actorD, target, damageRoll.total, damageType, [target], damageRoll, {flavor: `${itemD.name} - Damage Roll`, damageList: args[0].damageList, itemCardId: args[0].itemCardId});
}
