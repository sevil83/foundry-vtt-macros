//######################################
// Read First!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MidiQOL "on use" macro
// Remove damage from item, let the macro do it
//######################################
if(args[0].hitTargets.length > 0){
  let target = canvas.tokens.get(args[0].hitTargets[0]._id);
  let actorD = game.actors.get(args[0].actor._id);
  let undead = ["undead", "fiend"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
  let numDice = args[0].isCritical ? "4d6" : "2d6";
  let damageRoll;
  if (undead){
      damageRoll = new Roll(numDice).roll();
      game.dice3d?.showForRoll(damageRoll);
      new MidiQOL.DamageOnlyWorkflow(actorD, target, damageRoll.total, "radiant", [target], damageRoll, {flavor: `(Radiant)`, itemCardId: args[0].itemCardId});
  }
}
