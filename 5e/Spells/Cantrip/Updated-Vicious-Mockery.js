// Courtesy of @Zarek
// Selected target receives a random mockery from a table called "mockeries" along with the DC and damage.
// You can find a table called mockeries in the community tables module.


let tableName = "Mockeries";
// default mockery if no table found.
let mockery = "Now go away or I shall taunt you a second time-a!";

let viciousMockeries = () => {
  if (!actor) {
    ui.notifications.warn("You must have an actor selected.");
    return
  }

  let actorLevels = actor.data.data.levels || 1;
  let table = game.tables.entities.find(t => t.name == tableName);

  // Get Targets name
  const targetId = game.user.targets.ids[0];
  const targetToken = canvas.tokens.get(targetId);
  if (!targetToken) {
    ui.notifications.warn("You must target a token.");
    return
  }
  const targetName = targetToken.name;

  // Roll the result, and mark it drawn
  if (table) {
    if (checkTable(table)) {
      let roll = table.roll();
      let result = roll.results[0];
      mockery = result.text;
      table.updateEmbeddedEntity("TableResult", {
        _id: result._id,
        drawn: true
      });
    }
  }

  function checkTable(table) {
    let results = 0;
    for (let data of table.data.results) {
      if (!data.drawn) {
        results++;
      }
    }
    if (results < 1) {
      table.reset();
      ui.notifications.notify("Table Reset")
      return false
    }
    return true
  }

  // Add a message with damage roll
  let numDie = 1;
  if (actorLevels >= 17) {
    numDie = 4;
  } else if (actorLevels >= 11) {
    numDie = 3;
  } else if (actorLevels >= 5) {
    numDie = 2;
  }

  let messageContent = `<p>${token.name} exclaims <b><i>"${mockery}"</i></b></p>`; 

  // create the message
  if (messageContent !== '') {
    let chatData = {
      user: game.user._id,
      speaker: ChatMessage.getSpeaker(),
      content: messageContent,
    };
    ChatMessage.create(chatData, {});
  }
};
viciousMockeries();
game.dnd5e.rollItemMacro("Vicious Mockery");