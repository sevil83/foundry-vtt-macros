// Uses CUB and Checks for cub graphic on spell to goto next step of dealing damage.
// 2 stage spell like it's intended. Setup spell to cast on self with no damage assigned or upcasling.
let me = canvas.tokens.controlled[0];
let mSpell = item.actor.items.find(i=> i.name===`Produce Flame`);
let pfUpdate = duplicate(mSpell);

async function PF_update(pfUpdate){
await actor.updateEmbeddedEntity("OwnedItem", pfUpdate);
}

function PF_load(me, PF_update, pfUpdate){
pfUpdate.data.actionType = "rsak";
pfUpdate.data.damage.parts = [["1d8", "fire"]];
pfUpdate.data.target.type = "creature";
pfUpdate.data.target.value = "1";
pfUpdate.data.duration.value = null;
pfUpdate.data.duration.units = "inst";
pfUpdate.data.scaling.mode = "cantrip";
pfUpdate.data.scaling.formula = "1d8";
me.update({"dimLight": 10, "brightLight": 10, "lightColor": "#d6fcff",});
game.macros.getName("Add_Condition").execute(me.id, "Produce Flame"); 	
PF_update(pfUpdate);
}

function PF_reset(me, PF_update, pfUpdate){
pfUpdate.data.target.type = "self";
pfUpdate.data.target.value = null;
pfUpdate.data.duration.value = "10";
pfUpdate.data.duration.units = "minute";
pfUpdate.data.actionType = "util";
pfUpdate.data.damage.parts = [];
pfUpdate.data.scaling.mode = "none";
pfUpdate.data.scaling.formula = null;

PF_update(pfUpdate);
me.update({"dimLight": 0, "brightLight": 0, "lightColor": "",});
// CUB Apply Condition Call
game.macros.getName("Cub_Condition").execute(me.id, "Produce Flame", "add");
}

if (me.data.effects.includes("systems/dnd5e/icons/spells/explosion-red-1.jpg")) {
game.dnd5e.rollItemMacro("Produce Flame").then(()=>{
setTimeout(() => {PF_reset(me, PF_update, pfUpdate)}, 5000);
});
}
else {
game.dnd5e.rollItemMacro("Produce Flame").then(()=>{
setTimeout(() => {PF_load(me, PF_update, pfUpdate)}, 2000);
});
}