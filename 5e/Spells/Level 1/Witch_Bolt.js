// Midi-Qol Item Macro On Use Macro
async function wait(ms) {
	return new Promise(resolve => {
		setTimeout(resolve, ms);
	});
}
if (args[0].hitTargets.length > 0) {
	const target = await canvas.tokens.get(args[0].hitTargets[0]._id);
	const level = Number(args[0].spellLevel);
	const damageType = args[0].damageDetail[0].type;
	const itemD = args[0].item;
	const caster = game.actors.get(args[0].actor._id);
	if (target.inCombat) {
		const hookId = Hooks.on("updateCombat", combatRound);
		DAE.setFlag(target.actor, itemD.name, hookId);
	}

	async function witchboltStatus() {
		new Dialog({
			title: `${itemD.name}`,
			content: `<p>Continue concentrating?</p>`,
			buttons: {
				confirmed: { label: "Continue", callback: () => witchboltDamage() },
				cancel: { label: "Cancel It!", callback: () => witchboltCancel() }
			}
		}).render(true);
	}

	async function combatRound(combat, update) {
		if (!("round" in update || "turn" in update)) return;
		if (combat.combatant.token.actorId === caster.id) {
			await witchboltStatus();
		}
	}

	async function witchboltDamage() {
		let numDie = level;
		let damageRoll = new Roll(`${numDie}d12`).roll();
		game.dice3d?.showForRoll(damageRoll);
		new MidiQOL.DamageOnlyWorkflow(caster, target, damageRoll.total, damageType, [target], damageRoll, { flavor: `(${damageType})`, itemData: itemD, itemCardId: "new" });
	}

	async function witchboltCancel() {
		let ActiveEffects = game.macros.getName("ActiveEffect");
		let hookId = await DAE.getFlag(target.actor, itemD.name);
		let effect = caster.effects.find(i => i.data.label === "Concentrating");
		await Hooks.off("updateCombat", hookId);
		await DAE.unsetFlag(target.actor, itemD.name);
		await wait(500);
		ActiveEffects.execute(target.id, itemD.name, "remove");
		await caster.deleteEmbeddedEntity("ActiveEffect", effect.data._id);
	}
}
