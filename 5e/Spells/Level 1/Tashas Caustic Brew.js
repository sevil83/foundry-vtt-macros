// DAE macro. Set Macro repeat on "Start of turn".
if(args[0] === "each") {
    const lastArg = args[args.length-1];
    let token = canvas.tokens.get(lastArg.tokenId);
    let itemD = lastArg.efData.flags.dae.itemData;
    let level = itemD.data.level;
    let damageRoll = new Roll(`${level*2}d4`).roll();
    new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "acid", [token], damageRoll, {flavor: "(Acid)", itemData: itemD , itemCardId: "new"});
}
