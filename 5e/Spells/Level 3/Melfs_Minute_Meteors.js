// For this to work you need to a create another spell called "Melf's Minute Meteor" as an at-will spell, in addition to "Melf's Minute Meteors".
// "Melf's Minute Meteors" should be configured as a set to target self and remove all damage from it.
// This macro will use "Melf's Minute Meteors" as the main cast, then add charges to "Melf's Minute Meteor". Once the charges are gone, it will switch back to using the original spell.
// This macro uses Dynamic Effects to keep track of time and apply a self icon.

// DAE macro which finds Melf's Minute Meteor and adds charges to it.
const target = canvas.tokens.get(args[1]);
const level = Number(args[2]);
const mSpell = target.actor.items.find(i => i.name==="Melf's Minute Meteor");
const melfUpdate = duplicate(mSpell);
const Cub_Condition = game.macros.getName("Cub_Condition");
const melfs_on = target.actor.effects.find(i=> i.data.label === "Melf's Minute Meteors");
if(args[0]==="on") {
    update_melfs(target, melfUpdate, level);
}
if(args[0]==="off"){
reset_melfs(target, melfUpdate);
Cub_Condition.execute(target.id, "Concentrating", "remove");
}

async function update_melfs(target, melfUpdate, level) {
let update_level = level * 2;
melfUpdate.data.uses.value = update_level;
melfUpdate.data.uses.max = update_level;
await target.actor.updateEmbeddedEntity("OwnedItem", melfUpdate);
}

async function reset_melfs(target, melfUpdate) {
melfUpdate.data.uses.value = 0;
await target.actor.updateEmbeddedEntity("OwnedItem", melfUpdate);
}

// Melf's Minute Meteor Item Macro | Resets Spell if 0
(async()=>{
let actorD = game.actors.get(args[0].actor._id);
let itemD = args[0].item;
if(itemD.data.uses.value === 0){
    let effectId = actorD.effects.find(i=> i.data.label === "Melf's Minute Meteors").id;
    let conId = actorD.effects.find(i=> i.data.label === "Concentrating").id;
    await actorD.deleteEmbeddedEntity("ActiveEffect", effectId);
    await actorD.deleteEmbeddedEntity("ActiveEffect", conId);
}
})();

//Hotbar Macro
let melf_full = "Melf's Minute Meteors";
let melf_single = "Melf's Minute Meteor";
let me = canvas.tokens.controlled[0].actor || game.user.character;
if(me.effects.find(i=> i.data.label === melf_full)){
	game.dnd5e.rollItemMacro(melf_single);
}
else {
	game.dnd5e.rollItemMacro(melf_full);
}
