// Midi-qol On Use, asks player how much damage they just took. It sets a flag for 1 use per combat round.
(async ()=>{
  let target = await canvas.tokens.get(args[0].tokenId);
  let already_dodged = DAE.getFlag(target.actor, "uncanny_dodge");
  async function combatRound(combat, update) {
    if (!("round" in update || "turn" in update)) return;
    if (game.combat.current.tokenId === target.id) {
     const hookId = DAE.getFlag(target.actor, "uncanny_dodge");
      await DAE.unsetFlag(target.actor, "updateCombat", hookId);
    }
  }
  if(already_dodged) return ui.notifications.error(`You have already used this feat, this combat round.`);
  if(!already_dodged){
    new Dialog({
      title: "Ucanny Dodge",
      content: `<form><p>How much damage did you take?</p><div class="form-group"><label for="injury">Damage</label><input type="num" id="injury"></div></form>`,
      buttons: {
        one:  { label: "Submit", callback: async (html) => {
          let num = await html.find('#injury')[0].value;
          let healNum = Math.floor(Number(num/2));
          let healingRoll = new Roll(`{1d${healNum},${healNum}}kh`).roll();
          new MidiQOL.DamageOnlyWorkflow(actor, token, healingRoll.total, "healing", [target], healingRoll, {itemCardId: args[0].itemCardId});
          if (target.inCombat) {
            const hookId = Hooks.on("updateCombat", combatRound);
            await DAE.setFlag(target.actor, "uncanny_dodge", hookId);
          }
        }
      }
    }
  }).render(true);
}
})();
