// ItemMacro On Use, let this macro do the damage. Checks for hp total.
let itemD = args[0].item;
let actorD = args[0].actor;
if(args[0].hitTargets.length !== 0){
let target = canvas.tokens.get(args[0].hitTargets[0]._id);
let halfHp = actorD.data.attributes.hp.max/2;
let curtHp = actorD.data.attributes.hp.value;
let damageRoll = "";
curtHp <= halfHp ? damageRoll = new Roll('2d4').roll() : damageRoll = new Roll('4d4').roll();
new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "piercing", [target], damageRoll, {flavor: `${itemD.name} - Damage Roll (Piercing)`, itemCardId: args[0].itemCardId});
}
